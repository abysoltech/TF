# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.0.2"
    }
  }

  required_version = ">= 1.1.0"
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg"{
  name     = "mgn_rg"
  location = "eastus"

  tags = {
    Environment = "Terraform HCS Dev Env"
    Team = "MGN SRE DevOps"
    CreatedBy = "DevOps Terraform"
    Author = "mnaumowicz@vmware.com"
  }

}

# Create virtual network
resource "azurerm_virtual_network" "mgn_vnet" {
  name                = "mgn_vnet"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

# Create subnet
resource "azurerm_subnet" "mgn_sub" {
  name                 = "mgn_sub"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.mgn_vnet.name
  address_prefixes     = ["10.0.121.0/24"]
}

# Create public IPs
resource "azurerm_public_ip" "mgn_pip" {
  name                = "mgn_pip"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Dynamic"
}

# Create Bastion host for all VMs

resource "azurerm_subnet" "mgn_bastion_sub" {
  name                 = "AzureBastionSubnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.mgn_vnet.name
  address_prefixes     = ["10.0.121.224/27"]
}


resource "azurerm_bastion_host" "mgn_bastion" {
  name                = "mgn_bastion"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                 = "configuration"
    subnet_id            = azurerm_subnet.mgn_bastion_sub.id
    public_ip_address_id = azurerm_public_ip.mgn_pip.id
  }
}


# Create Network Security Group and rule
resource "azurerm_network_security_group" "mgn_nsg" {
  name                = "mgn_nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

# Create network interface
resource "azurerm_network_interface" "mgn_nic" {
  name                = "mgn_nic"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "mgn_ip_cfg"
    subnet_id                     = azurerm_subnet.mgn_sub.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.mgn_pip.id
  }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "mgn_nsg_asoc" {
  network_interface_id      = azurerm_network_interface.mgn_nic.id
  network_security_group_id = azurerm_network_security_group.mgn_nsg.id
}

# Generate random text for a unique storage account name
resource "random_id" "randomId" {
  keepers = {
    # Generate a new ID only when a new resource group is defined
    resource_group = azurerm_resource_group.rg.name
  }

  byte_length = 8
}

# Create storage account for boot diagnostics
resource "azurerm_storage_account" "mgn_stor" {
  name                     = "diag${random_id.randomId.hex}"
  location                 = azurerm_resource_group.rg.location
  resource_group_name      = azurerm_resource_group.rg.name
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

# Create my own key vault
resource "azurerm_key_vault" "mgn_keyvault" {
  name = "kv-mgn-vault"
  tenant_id = var.tenant_id
  sku_name = "standard"
  resource_group_name = azurerm_resource_group.rg.name
  location = azurerm_resource_group.rg.location
}

# Pull ssh key from the vault
#resource "azurerm_key_vault_secret" "my_ssh_key" {
#  name  = "mgn_ssh_key"
#  value = azurerm_key_vault.mgn_keyvault.mgn_ssh_key
#}
   

# Create virtual machine
resource "azurerm_linux_virtual_machine" "mgn-vm" {
  name                  = "mgn-vm"
  location              = azurerm_resource_group.rg.location
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.mgn_nic.id]
  size                  = "Standard_DS1_v2"
  computer_name                   = "mgn-vm"
  admin_username                  = "DaBoss"
  disable_password_authentication = true
  custom_data = filebase64("cloud-init.txt")

  admin_ssh_key {
    username   = "DaBoss"
    public_key = file("~/.ssh/id_rsa.pub")
  }

  os_disk {
    name                 = "mgn_disk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.mgn_stor.primary_blob_endpoint
  }
}
