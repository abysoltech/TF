output "resource_group_id" {
  value = azurerm_resource_group.rg.id
}

output "public_ip_address" {
  value = "${azurerm_public_ip.mgn_pip.ip_address}"
}

output "vm_name" {
  value = "${azurerm_linux_virtual_machine.mgn-vm.name}"
}

